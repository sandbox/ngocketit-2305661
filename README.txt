Node disclaimer module enables the protection of node pages with a disclaimer

Requirements
============

This module requires Drupal 7 and CTools module

Installation
============

1. Unzip the module into /sites/all/modules or /sites/all/contrib/modules
2. Enable the module on the Modules page
3. To create the disclaimer and configure the module, go to 
   Configuration -> Content authoring -> Node disclaimer
