/**
 * @file
 * Processes the protected page & disclaimer page accordingly.
 */

(function($) {
  'use strict';

  Drupal.NodeDisclaimer = function() {
    var self = this,
        node_settings = {},
        disclaimer_settings = {};

    /**
     * Accept the disclaimer and save the information to cookie.
     */
    this.acceptDisclaimer = function() {
      var cookie_name = this.getCookieName(disclaimer_settings.did, disclaimer_settings.nid, disclaimer_settings.dvid);
      $.cookie(cookie_name, 1, { expires: parseFloat(disclaimer_settings.validity), path: '/' });
      this.redirect(Drupal.settings.basePath + 'node/' + disclaimer_settings.nid);
    };

    /**
     * Disagree the disclaimer and redirect to certain URL configured by admin.
     */
    this.refuseDisclaimer = function() {
      this.redirect(disclaimer_settings.disagree_redirect);
    };

    /**
     * Handle the disclaimer node and do the necessary redirect accordingly.
     */
    this.handleDisclaimer = function() {
      var nid = location.hash.replace('#', ''),
          cookie_name = this.getCookieName(disclaimer_settings.did, nid),
          selector, triggers;
      disclaimer_settings.nid = nid;

      var bindElementHandlers = function(selectors, triggers, callback) {
        $(selector).bind(triggers, function(event) {
          event.preventDefault();
          callback();
        });
      };

      if (disclaimer_settings.accept_element) {
        selector = disclaimer_settings.accept_element.selector;
        triggers = disclaimer_settings.accept_element.trigger.join(' ');

        $(selector).bind(triggers, function(event) {
          event.preventDefault();
          self.acceptDisclaimer();
        });
      }

      if (disclaimer_settings.refuse_element) {
        selector = disclaimer_settings.refuse_element.selector;
        triggers = disclaimer_settings.refuse_element.trigger.join(' ');

        $(selector).bind(triggers, function(event) {
          event.preventDefault();
          self.refuseDisclaimer();
        });
      }
    }

    /**
     * Get the name of the cookie for given node & disclaimer pair.
     */
    this.getCookieName = function(did, nid, dvid) {
      return 'node_disclaimer_' + did + '_' + dvid + '_' + nid;
    }

    this.handleNode = function() {
      // First, check if the disclaimer can be applied to current domain.
      if (node_settings.domains) {
        var domains = node_settings.domains.split(',');
        if (domains.length > 0) {
          if (domains.indexOf(location.host) < 0) {
            return;
          }
        }
      }

      var cookie_name = this.getCookieName(node_settings.did, node_settings.nid, node_settings.dvid);
      if ($.cookie(cookie_name) == undefined) {
        this.redirect(node_settings.redirect);
        return;
      }

      $('body').removeClass('element-invisible').attr('style', 'visibility: visible !important');
    }

    /**
     * Redirect to a new URL
     */
    this.redirect = function(url) {
      window.location = url;
    }

    /**
     * Entry point of any further checking.
     */
    this.init = function() {
      // This is not the node we're interested in.
      if (!Drupal.settings.node_disclaimer) {
        return;
      }

      var settings = Drupal.settings.node_disclaimer;

      // This is a disclaimer node.
      if (settings.disclaimer) {
        disclaimer_settings = settings.disclaimer;
        this.handleDisclaimer();
      }

      // And a node protected by a disclaimer.
      else if (settings.node) {
        node_settings = settings.node;
        this.handleNode();
      }

      return this;
    };

    return this.init();
  };

  /**
   * Behavior for checking disclaimer and redirect user if needed.
   */
  Drupal.behaviors.nodeDisclaimer = {
    attach: function(context, settings) {
      var node_disclaimer = new  Drupal.NodeDisclaimer();
      node_disclaimer.init();
    }
  };

})(jQuery);
