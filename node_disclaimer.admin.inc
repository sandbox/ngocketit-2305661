<?php

/**
 * @file
 * Contains administrative features for Node disclaimer module.
 */


/**
 * Validation handler for disclaimer add/edit form.
 */
function node_disclaimer_disclaimer_form_validate($form, &$form_state) {
  $disclaimer = $form_state['values']['disclaimer'];
  $matches = array();

  // Are we creating a new disclaimer?.
  if (!isset($form_state['values']['disclaimer_nid'])) {
    if (!preg_match('/\[([0-9]+)\]$/', $disclaimer, $matches)) {
      form_set_error('disclaimer', t('Please check & make sure that the node title is entered correctly'));
    }
    else {
      // Then make sure that the node is existing and accessible.
      $node_id = $matches[1];
      if (!($node = node_load($node_id)) || $node->status != NODE_PUBLISHED) {
        form_set_error('disclaimer', t('Node with ID %nid can not be found.', array('%nid' => $node_id)));
      }

      $form_state['values']['disclaimer_nid'] = $node_id;
    }
  }

  $validity = $form_state['values']['validity'];
  if (!is_numeric($validity) || $validity < 0) {
    form_set_error('validity', t('Please specify the correct validity value'));
  }
}


/**
 * Submit handler for disclaimer add/edit form.
 */
function node_disclaimer_disclaimer_form_submit($form, &$form_state) {
  $disclaimer_nid = $form_state['values']['disclaimer_nid'];

  $result = db_select('node_disclaimer_disclaimer', 'd')
    ->condition('nid', $disclaimer_nid)
    ->fields('d')
    ->execute()
    ->fetchAssoc();

  $enabled = $form_state['values']['enabled'] ? NODE_DISCLAIMER_ENABLED : NODE_DISCLAIMER_DISABLED;
  $record = array(
    'nid' => $disclaimer_nid,
    'enabled' => $enabled,
    'validity' => $form_state['values']['validity'],
    'form_plugin' => $form_state['values']['form_plugin'],
    'settings' => array(
      'disagree_redirect' => $form_state['values']['disagree_redirect'],
    ),
  );

  if (module_exists('domain')) {
    $domains = $form_state['values']['domains'];
    $selected_domains = array();

    foreach ($domains as $name => $checked) {
      if ($checked) {
        $selected_domains[] = $name;
      }
    }
    if (!empty($selected_domains)) {
      $record['domains'] = implode(',', $selected_domains);
    }
  }

  if (!$result) {
    drupal_write_record('node_disclaimer_disclaimer', $record);
  }
  else {
    drupal_write_record('node_disclaimer_disclaimer', $record, 'nid');
  }

  drupal_set_message(t('Disclaimer saved'));
}


/**
 * Node disclaimer view form listing all protected nodes.
 */
function node_disclaimer_disclaimer_view_form($form, &$form_state, $disclaimer) {
  drupal_set_title(t('Disclaimer: @t', array('@t' => $disclaimer->title)));

  $headers = array();
  $headers[] = array('data' => t('Title'), 'field' => 'title', 'sort' => 'asc');
  $headers[] = array('data' => t('Type'), 'field' => 'nt.name', 'sort' => 'asc');
  $headers[] = array(
    'data' => t('Author'),
    'field' => 'u.name',
    'sort' => 'asc',
  );
  $headers[] = array(
    'data' => t('Published'),
    'field' => 'status',
    'sort' => 'asc',
  );
  $headers[] = array(
    'data' => t('Operations'),
  );

  $table = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => array(),
    '#empty' => t('No protected nodes.'),
  );

  $query = db_select('node_disclaimer_node', 'dn');
  $query->join('node', 'n', 'n.nid = dn.nid');
  $query->join('users', 'u', 'n.uid = u.uid');
  $query->join('node_type', 'nt', 'nt.type = n.type');

  $query = $query->extend('TableSort')->extend('PagerDefault');
  $query->addField('u', 'name', 'authorname');
  $query->addField('nt', 'name', 'node_type');
  $result = $query->fields('n')
    ->condition('dn.did', $disclaimer->nid)
    ->orderByHeader($headers)
    ->execute();

  $can_view_author = user_access('access user profiles');

  foreach ($result as $node) {
    $table['#rows'][] = array(
      l($node->title, 'node/' . $node->nid),
      check_plain($node->node_type),
      $can_view_author ? l($node->authorname, 'users/' . $node->authorname) : check_plain($node->authorname),
      $node->status == NODE_PUBLISHED ? t('Yes') : t('No'),
      l(t('Delete'), NODE_DISCLAIMER_BASE_PATH . '/node-delete/' . $node->nid . '/' . $disclaimer->nid),
    );
  }

  $form['protected'] = $table;

  $form['disclaimer_nid'] = array(
    '#type' => 'value',
    '#value' => $disclaimer->nid,
  );

  $form['new_node'] = array(
    '#type' => 'textfield',
    '#title' => t('Add a new node'),
    '#description' => t('Enter the title of the node that you want to be protected by this disclaimer.'),
    '#size' => 60,
    '#autocomplete_path' => 'node_disclaimer/node_autocomplete',
  );

  $form['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}


/**
 * Validate handler for disclaimer view form.
 */
function node_disclaimer_disclaimer_view_form_validate($form, &$form_state) {
  $matches = array();

  if (!preg_match('/\[([0-9]+)\]$/', $form_state['values']['new_node'], $matches)) {
    form_set_error('new_node', t('Please check & make sure that the node title is entered correctly'));
  }
  else {
    $form_state['values']['node_nid'] = $matches[1];
  }
}


/**
 * Submit handler for disclaimer view form.
 */
function node_disclaimer_disclaimer_view_form_submit($form, &$form_state) {
  db_insert('node_disclaimer_node')
    ->fields(array(
      'did' => $form_state['values']['disclaimer_nid'],
      'nid' => $form_state['values']['node_nid'],
    ))
    ->execute();

  drupal_set_message(t('Node has been added.'));
}


/**
 * Auto complete search for disclaimer node.
 */
function node_disclaimer_autocomplete_search($search_type, $string = '') {
  if (empty($string)) {
    return;
  }

  $matches = array();

  // Check if we have restricted disclaimer to certain node types.
  $types = variable_get("node_disclaimer_{$search_type}_types", array());
  $allowed_types = array();

  foreach ($types as $type => $status) {
    if ($status) {
      $allowed_types[] = $type;
    }
  }

  $query = db_select('node')
    ->fields('node', array('nid', 'title'))
    ->condition('status', NODE_PUBLISHED)
    ->condition('title', db_like($string) . '%', 'LIKE');

  // Limit the search to configured types only.
  if (!empty($allowed_types)) {
    $query->condition('type', $allowed_types, 'IN');
  }

  $excluded = array();

  // Make sure that we don't add duplicated nodes.
  $table = 'node_disclaimer_' . ($search_type == 'disclaimer' ? 'disclaimer' : 'node');
  $result = db_select($table, 'd')
    ->fields('d', array('nid'))
    ->execute();

  foreach ($result as $node) {
    $excluded[] = $node->nid;
  }

  if (!empty($excluded)) {
    $query->condition('nid', $excluded, 'NOT IN');
  }

  $result = $query->execute();

  foreach ($result as $node) {
    $matches[$node->title . " [$node->nid]"] = check_plain($node->title);
  }

  return drupal_json_output($matches);
}


/**
 * Auto complete search for disclaimer node.
 */
function node_disclaimer_node_autocomplete($string = '') {
  return node_disclaimer_autocomplete_search('protected', $string);
}


/**
 * Auto complete search for disclaimer node.
 */
function node_disclaimer_disclaimer_autocomplete($string = '') {
  return node_disclaimer_autocomplete_search('disclaimer', $string);
}


/**
 * Disclaimer delete confirmation form.
 */
function node_disclaimer_disclaimer_node_delete_form($form, &$form_state, $node, $disclaimer) {
  $form['node_id'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );

  $form['disclaimer_id'] = array(
    '#type' => 'value',
    '#value' => $disclaimer->nid,
  );

  $destination = isset($_GET['destination']) ? $_GET['destination'] : NODE_DISCLAIMER_BASE_PATH . '/view/' . $disclaimer->nid;

  return confirm_form($form,
    t('Are you sure you want to remove disclaimer protection for the node "%t"?', array('%t' => $node->title)),
    $destination,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}


/**
 * Submit handler for node delete form.
 */
function node_disclaimer_disclaimer_node_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $did = $form_state['values']['disclaimer_id'];
    $nid = $form_state['values']['node_id'];

    db_delete('node_disclaimer_node')
      ->condition('did', $did)
      ->condition('nid', $nid)
      ->execute();

    drupal_set_message(t('Node has been removed form disclaimer protection'));

    $form_state['redirect'] = NODE_DISCLAIMER_BASE_PATH . '/view/' . $did;
  }
}
