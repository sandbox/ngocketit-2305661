<?php

/**
 * @file
 * Documentation for node_disclaimer API & hooks.
 */


/**
 * Alter all node disclaimer forms.
 *
 * @param object $form
 *   The disclaimer form to alter.
 */
function hook_node_disclaimer_form_alter(&$form) {
  if (isset($form['button']) && $form['button']['#type'] == 'submit') {
    $form['button']['#value'] = t('I agree with the disclaimer');
  }
}


/**
 * Alter node disclaimer form for certain plugin.
 *
 * The PLUGIN_CLASS_NAME is formed by replacing the capitalized characters in
 * the class name with an underscore. For example, class
 * NodeDisclaimerFormButton will result in node_disclaimer_form_button and the
 * corresponding hook will be: hook_node_disclaimer_form_button_alter().
 *
 * @param object $form
 *   The disclaimer form to alter.
 */
function hook_PLUGIN_CLASS_NAME_alter(&$form) {
  $form['checkbox']['#title'] = t('I agree with all above terms');
}
