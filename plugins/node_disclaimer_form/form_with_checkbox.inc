<?php
/**
 * @file
 * A plugin that defines the disclaimer form with checkbox.
 */

$plugin = array(
  'label' => t('Disclaimer form with checkboxes'),
  'handler' => array(
    'class' => 'NodeDisclaimerFormCheckbox',
  ),
);


class NodeDisclaimerFormCheckbox extends NodeDisclaimerForm {
  /**
   * Render disclaimer form.
   */
  public function disclaimerForm() {
    $form = $this->defaultDisclaimerForm();

    $form['agree'] = array(
      '#type' => 'checkbox',
      '#id' => 'disclaimer-accept',
      '#title' => t('I acknowledge and accept disclaimer'),
    );

    return $form;
  }

  /**
   * Get the form element that triggers the acceptance.
   */
  public function getAcceptElement() {
    return array(
      'selector' => '#disclaimer-accept',
      'trigger'  => array('click'),
    );
  }
}
