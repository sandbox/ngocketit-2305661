<?php
/**
 * @file
 * A plugin that defines the disclaimer form with checkbox & button.
 */

$plugin = array(
  'label' => t('Disclaimer form with checkbox & button'),
  'handler' => array(
    'class' => 'NodeDisclaimerFormCheckboxButton',
  ),
);


class NodeDisclaimerFormCheckboxButton extends NodeDisclaimerForm {
  /**
   * Render disclaimer form.
   */
  public function disclaimerForm() {
    $form = $this->defaultDisclaimerForm();

    $form['checkbox'] = array(
      '#type' => 'checkbox',
      '#id' => 'disclaimer-checkbox',
      '#title' => t('I acknowledge and accept the terms'),
    );

    $form['button'] = array(
      '#type' => 'button',
      '#id' => 'disclaimer-button',
      '#value' => t('Continue'),
    );

    $module_path = drupal_get_path('module', 'node_disclaimer');

    $form['#attached'] = array(
      'js' => array(
        $module_path . '/plugins/node_disclaimer_form/checkbox_button.js',
      ),
    );

    return $form;
  }
}
