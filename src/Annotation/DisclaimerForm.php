<?php

/**
 * @file
 *
 * Contains \Drupal\node_disclaimer\DisclaimerType.
 */

namespace Drupal\node_disclaimer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for disclaimer type plugin.
 *
 * @Annotation
 */
class DisclaimerForm extends Plugin {
  /**
   * The plugin label.
   *
   * @var string
   */
  public $label;
}
