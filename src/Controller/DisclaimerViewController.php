<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\Controller\DisclaimerViewController.
 */

namespace Drupal\node_disclaimer\Controller;

use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\node_disclaimer\DisclaimerInterface;

/**
 * Defines a controller to render a disclaimer.
 */
class DisclaimerViewController extends EntityViewController {

  /**
   * {@inheritdoc}
   *
   * NOTE: The name of the entity parameter is important and has to be the same
   * as the entity type.
   */
  public function view(DisclaimerInterface $node_disclaimer, $view_mode = 'full', $langcode = NULL) {
    return parent::view($node_disclaimer, $view_mode, $langcode);
  }

}
