<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\NodeDisclaimerController.
 */

namespace Drupal\node_disclaimer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for admin features of the module.
 */
class NodeDisclaimerController extends ControllerBase {

  public function listDisclaimer() {
  }

  public function addDisclaimer() {

  }

  public function editDisclaimer(DisclaimerInterface $disclaimer) {

  }

  public function deleteDisclaimer(DisclaimerInterface $disclaimer) {
  }
}

