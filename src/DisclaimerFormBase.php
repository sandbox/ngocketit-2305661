<?php

/**
 * @file
 *
 * Contains \Drupal\node_disclaimer\DisclaimerTypeBase.
 */

namespace Drupal\node_dsiclaimer;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Plugin\PluginBase;

/**
 * Defines a base class for all disclaimer type plugins.
 */
abstract class DisclaimerFormBase extends PluginBase implements DisclaimerFormInterface {
  use StringTranslationTrait;

  /**
   * Module handler.
   */
  protected $module_handler;

  public function __construct() {
    $this->module_handler = \Drupal::moduleHandler();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $form = $this->disclaimerForm();

    // Allow other modules to alter the form.
    $class_name = ucfirst(get_class($this));
    $class_name = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class_name));

    // Other modules can either use hook_node_disclaimer_form_alter() or
    // hook_{$class_name}_alter() to alter disclaimer form. Use the later one
    // if you want to alter the more specific form, for example to alter the
    // form generated by NodeDisclaimerFormCheckboxButton plugin, use
    // hook_node_disclaimer_form_checkbox_button_alter().
    $alter_types = array(
      'node_disclaimer_form',
      $class_name,
    );
    $this->moduleHandler->alter($alter_types, $form);

    $plugin_id = $this->getPluginId();
    $wrapper_attributes = array(
      'class' => array(
        'node-disclaimer-form',
        strtr($plugin_id, '_', '-'),
      ),
    );

    $render_array = array(
      '#theme' => 'disclaimer_form',
      '#attributes' => array(
        'class' => $wrapper_attributes,
      ),
      '#form' => $form,
    );

    return $render_array;
  }


  /**
   * {@inheritdoc}
   */
  protected function disclaimerForm() {
    // Lets have a default type for disagree button and other plugins can
    // optionally include it in their form definitions.
    $form['disagree_button'] = array(
      '#type' => 'button',
      '#id' => 'disclaimer-disagree',
      '#value' => t('Disagree'),
    );

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  abstract public function getAcceptElement();


  /**
   * {@inheritdoc}
   */
  public function getRefuseElement() {
    return array(
      'selector' => '#disclaimer-disagree',
      'trigger' => array('click'),
    );
  }
}
