<?php

/**
 * @file
 *
 * Contains \Drupal\node_disclaimer\DisclaimerTypeInterface.
 */

namespace Drupal\node_disclaimer;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for all disclaimer type plugins.
 */
interface DisclaimerFormInterface extends PluginInspectionInterface {
  /**
   * Renders the disclaimer form.
   * @return string
   *   The HTML output of the disclaimer form.
   */
  public function render();

  /**
   * Get the disclaimer form.
   *
   * @return object
   *   The form representing the disclaimer
   */
  public function disclaimerForm();


  /**
   * Get the form element that triggers the acceptance of disclaimer.
   *
   * @return array
   *   The array of the following items:
   *   - selector: A selector that allows getting the element object
   *   using jQuery DOM selection methods.
   *   - trigger: An array of events that once fired will trigger the
   *   acceptance of disclaimer.
   */
  public function getAcceptElement();


  /**
   * Get the form element that triggers the refusal of disclaimer.
   *
   * @return array
   *   The array of the following items:
   *   - selector: A selector that allows getting the element object
   *   using jQuery DOM selection methods.
   *   - trigger: An array of events that once fired will trigger the
   *   refusal of disclaimer.
   */
  public function getRefuseElement();
}
