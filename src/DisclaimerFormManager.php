<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\DisclaimerFormManager.
 */

namespace Drupal\node_disclaimer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Defines a plugin manager class for disclaimer form plugins.
 */
class DisclaimerFormManager extends DefaultPluginManager {
  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $plugin_interface = 'Drupal\node_disclaimer\DisclaimerFormInterface';
    $plugin_annotation = 'Drupal\node_disclaimer\Annotation\DisclaimerForm';

    parent::__construct('Plugin/DisclaimerForm', $namespaces, $module_handler, $plugin_interface, $plugin_annotation);

    $this->alterInfo('disclaimer_form_info');
    $this->setCacheBackend($cache_backend, 'disclaimer_form');
  }
}
