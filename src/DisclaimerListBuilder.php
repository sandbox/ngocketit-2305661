<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\DisclaimerListBuilder.
 */

namespace Drupal\node_disclaimer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for showing list of disclaimers.
 */
class DisclaimerListBuilder extends ConfigEntityListBuilder {

  /**
   * Disclaimer form plugin manager.
   *
   * @var \Drupal\node_disclaimer\DisclaimerFormManagerInterface;
   */
  protected $disclaimerFormManager;


  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.disclaimer_form')
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DisclaimerFormManagerInterface $disclaimerFormManager) {
    parent::__construct($entity_type, $storage);
    $this->disclaimerFormManager = $disclaimerFormManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['enabled'] = $this->t('Enabled');
    $header['validity'] = $this->t('Validity (days)');
    $header['form'] = $this->t('Form');
    $header['protected_nodes'] = $this->t('Protected nodes');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $controller = \Drupal::entityManager()->getStorage('node');
    $node = $controller->load($entity->nid);

    if ($node) {
      $row['name']['data'] = array(
        '#type' => 'link',
        '#title' => $node->label(),
        '#url' => $node->urlInfo(),
      );
    }
    else {
      $row['name'] = $this->t('N/A');
    }

    $plugin = $this->disclaimerFormManager->getDefinition($entity->form_plugin);

    $row['enabled'] = $entity->enabled ? $this->t('Yes') : $this->t('No');
    $row['validity'] = $entity->validity;
    $row['form'] = $plugin['label'];
    $row['protected_nodes'] = count($entity->protected_nodes);

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $operations['view'] = array(
      'title' => t('View'),
      'weight' => -15,
      'url' => $entity->urlInfo('canonical'),
    );

    return $operations;
  }

}
