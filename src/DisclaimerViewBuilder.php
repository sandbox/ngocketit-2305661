<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\DisclaimerViewBuilder.
 */

namespace Drupal\node_disclaimer;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Form\FormManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the view builder for disclaimer entity. This view builder will be
 * called by an EntityViewController (DisclaimerViewController) automatically.
 */
class DisclaimerViewBuilder extends EntityViewBuilder {

  /**
   * Form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInerface.
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, FormBuilderInterface $form_builder) {
    parent::__construct($entity_type, $entity_manager, $language_manager);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager'),
      $container->get('language_manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    // Render the disclaimer view form.
    return $this->formBuilder->getForm('Drupal\node_disclaimer\Form\DisclaimerViewForm', $entity);
  }

}
