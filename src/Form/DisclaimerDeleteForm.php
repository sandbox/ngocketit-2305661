<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\Form\DisclaimerDeleteForm.
 */

namespace Drupal\node_disclaimer\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete a disclaimer entity.
 */
class DisclaimerDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name disclaimer?', array(
      '%name' => $this->entity->label(),
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.node_disclaimer.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->entity->delete()) {
      drupal_set_message($this->t('Disclaimer %name has been deleted.', array(
        '%name' => $this->entity->label(),
      )));
    }
    else {
      drupal_set_message($this->t('Failed to delete disclaimer %name.', array(
        '%name' => $this->entity->label(),
      )), 'error');
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
