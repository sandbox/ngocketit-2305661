<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\Form\DisclaimerDeleteNodeForm.
 */

namespace Drupal\node_disclaimer\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Defines a class for deleting a protected node.
 */
class DisclaimerDeleteNodeForm extends ConfirmFormBase {

  /**
   * Disclaimer object.
   *
   * @var \Drupal\node_disclaimer\DisclaimerInterface.
   */
  protected $disclaimer;

  /**
   * Protected node to be removed.
   *
   * @var \Drupal\node\NodeInterface.
   */
  protected $node;

  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface.
   */
  protected $entityManager;

  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you really want unprotect this node %title?', array(
      '%title' => $this->node->label(),
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disclaimer_node_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.node_disclaimer.canonical', array(
      'node_disclaimer' => $this->disclaimer->id(),
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $disclaimer_id = NULL, $nid = NULL) {
    $this->disclaimer = $this->entityManager->getStorage('node_disclaimer')->load($disclaimer_id);
    $this->node = $this->entityManager->getStorage('node')->load($nid);

    if (!$this->disclaimer || !$this->node) {
      drupal_set_message(t('Either disclaimer or protected node is invalid. Please make sure the URL is correct.'), 'error');
      return NULL;
    }

    $form['disclaimer_id'] = array(
      '#type' => 'value',
      '#value' => $disclaimer_id,
    );

    $form['nid'] = array(
      '#type' => 'value',
      '#value' => $nid,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $disclaimer = $this->entityManager->getStorage('node_disclaimer')->load($form_state->getValue('disclaimer_id'));
    $node = $this->entityManager->getStorage('node')->load($form_state->getValue('nid'));

    if ($disclaimer && $node) {
      $protected_nodes = $disclaimer->protected_nodes;
      $index = array_search($node->id(), $protected_nodes);

      if ($index >= 0) {
        unset($protected_nodes[$index]);
        $disclaimer->protected_nodes = $protected_nodes;
        $disclaimer->save();
        drupal_set_message(t('Node %title has been unprotected'), array(
          '%title' => $node->label(),
        ));
      }
    }
    else {
      drupal_set_message(t('Either disclaimer or node is invalid.'), 'error');
    }

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
