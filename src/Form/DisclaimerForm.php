<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\Form\DisclaimerForm.
 */

namespace Drupal\node_disclaimer\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node_disclaimer\DisclaimerFormManager;

/**
 * The definition of a Disclaimer configuration entity add/edit form.
 */
class DisclaimerForm extends EntityForm {

  /**
   * Entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Disclaimer form plugin manager.
   *
   * @var \Drupal\node_disclaimer\DisclaimerFormPluginManager.
   */
  protected $disclaimerFormManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityManagerInterface $entityManager, DisclaimerFormManager $disclaimerFormManager) {
    $this->entityManager = $entityManager;
    $this->disclaimerFormManager = $disclaimerFormManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('plugin.manager.disclaimer_form')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $disclaimer = $this->entity;

    $form['nid'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Disclaimer node'),
      '#description' => $this->t('Enter title of the node for autocomplete search'),
      '#required' => TRUE,
      '#maxlength' => 255,
      '#size' => 80,
      '#autocomplete_route_name' => 'node_disclaimer.disclaimer_autocomplete',
    );

    // Make sure that autocomplete search is only enabled on add form.
    if ($this->operation == 'edit') {
      unset($form['disclaimer']['#autocomplete_route_name']);
      $form['nid']['#disabled'] = TRUE;
      $form['nid']['#default_value'] = $disclaimer->label();

      $form['disclaimer_nid'] = array(
        '#type' => 'hidden',
        '#value' => $disclaimer->nid,
      );
    }

    $plugins = $this->disclaimerFormManager->getDefinitions();
    $form_options = array(
      '' => $this->t('- Select form type -'),
    );

    foreach ($plugins as $definition) {
      $form_options[$definition['id']] = $definition['label'];
    }

    $form['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#description' => $this->t('If checked, the disclaimer protection will be enabled.'),
      '#default_value' => $disclaimer ? $disclaimer->enabled : TRUE,
    );

    $form['form_plugin'] = array(
      '#type' => 'select',
      '#title' => $this->t('Disclaimer form type'),
      '#description' => $this->t('Select how the disclaimer acceptance form is represented'),
      '#options' => $form_options,
      '#required' => FALSE,
      '#default_value' => $disclaimer ? $disclaimer->form_plugin : FALSE,
    );

    $form['validity'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Validity'),
      '#size' => 10,
      '#description' => $this->t('Specify the number of days the protection remains valid before user has to accept the disclaimer again.<br/>Enter zero (0) to make the disclaimer valid until users close the browser.'),
      '#default_value' => !$disclaimer ? 7 : $disclaimer->validity,
    );

    if (\Drupal::moduleHandler()->moduleExists('domain')) {
      $domains = domain_domains();
      $form['domains'] = array(
        '#type' => 'checkboxes',
        '#title' => $this->t('Domains'),
        '#description' => $this->t('Select domains on which the disclaimer is applied'),
        '#options' => $domains,
      );
    }

    $form['disagree_redirect'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Disagree redirect URL'),
      '#maxlength' => 255,
      '#description' => $this->t('URL to redirect users to when they do not access the disclaimer. Leave it empty or use &lt;front&gt; to redirect to the frontpage'),
      '#default_value' => isset($settings['disagree_redirect']) ? $settings['disagree_redirect'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $form, FormStateInterface $form_state) {
    parent::validate($form, $form_state);

    if ($this->entity->isNew()) {
      if (!preg_match('/\[([0-9]+)\]$/', $form_state->getValue('nid'), $matches)) {
        $form_state->setErrorByName('nid', $this->t('Please check & make sure that the node title is entered correctly'));
      }
      else {
        // Then make sure that the node is existing and accessible.
        $node_id = intval($matches[1]);

        if (!$this->entityManager->getStorage('node')->load($node_id)) {
          $form_state->setErrorByName('nid', $this->t('Node with ID %nid can not be found.', array('%nid' => $node_id)));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if ($this->entity->isNew()) {
      if (preg_match('/\[([0-9]+)\]$/', $form_state->getValue('nid'), $matches)) {
        $this->entity->nid = intval($matches[1]);
      }
    }
    else {
      $this->entity->nid = intval($form_state->getValue('disclaimer_nid'));
    }

    parent::save($form, $form_state);
    drupal_set_message(t('Disclaimer saved.'));
  }
}
