<?php

/**
 * @file
 * Defines a disclaimer view form.
 */

namespace Drupal\node_disclaimer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Url;

/**
 * Defines a form for disclaimer view.
 */
class DisclaimerViewForm extends FormBase {

  /**
   * Entity query service.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface.
   */
  protected $entityManager;

  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disclaimer_view_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $disclaimer = NULL) {
    $headers = array();

    $headers[] = array('data' => t('Title'), 'field' => 'title', 'sort' => 'asc');
    $headers[] = array('data' => t('Type'), 'field' => 'nt.title', 'sort' => 'asc');
    $headers[] = array('data' => t('Author'), 'field' => 'u.name', 'sort' => 'asc');
    $headers[] = array('data' => t('Published'), 'field' => 'status', 'sort' => 'asc');
    $headers[] = array('data' => t('Operations'));

    $table = array(
      '#tree' => TRUE,
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => array(),
      '#empty' => t('No protected nodes with this disclaimer'),
    );


    if (($nids = $disclaimer->protected_nodes)) {
      // There are several ways to load the node entities but since we already
      // have entity.manager service, use it anyway.
      $protected_nodes = $this->entityManager->getStorage('node')->loadMultiple($nids);

      foreach ($protected_nodes as $node) {
        $node_link = array(
          'data' => array(
            '#type' => 'link',
            '#title' => SafeMarkup::checkPlain($node->label()),
            '#url' => $node->urlInfo('canonical'),
          ),
        );

        $operations = array(
          'data' => array(
            '#type' => 'operations',
            '#links' => array(
              array(
                'title' => t('Delete'),
                'url' => new Url('node_disclaimer.node_delete', array(
                  'disclaimer_id' => $disclaimer->id(),
                  'nid' => $node->id(),
                )),
              ),
            ),
          ),
        );

        $row = array(
          $node_link,
          SafeMarkup::checkPlain($node->bundle()),
          $node->getRevisionAuthor()->label(),
          $node->status == NODE_PUBLISHED ? t('Yes') : t('No'),
          $operations,
        );

        $table['#rows'][] = $row;
      }
    }

    $form['table'] = $table;

    $form['disclaimer_nid'] = array(
      '#type' => 'value',
      '#value' => $disclaimer->id(),
    );

    $form['new_node'] = array(
      '#type' => 'textfield',
      '#title' => t('Add a new node'),
      '#description' => t('Enter the title of the node that you want to protect.'),
      '#size' => 60,
      '#autocomplete_route_name' => 'node_disclaimer.node_autocomplete',
      '#autocomplete_route_parameters' => array('disclaimer_id' => $disclaimer->id()),
    );

    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $disclaimer_id = $form_state->getValue('disclaimer_nid');
    $disclaimer = $this->entityManager->getStorage('node_disclaimer')->load($disclaimer_id);

    if (!$disclaimer) {
      drupal_set_message(t('Disclaimer can not be loaded. Please make sure it exists.'));
      return $form;
    }

    if (preg_match('/\[([0-9]+)\]$/', $form_state->getValue('new_node'), $matches)) {
      $nid = intval($matches[1]);
      $disclaimer->protected_nodes[] = $nid;
      $disclaimer->save();
      drupal_set_message(t('Disclaimer saved.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array $form, FormStateInterface $form_state) {
    parent::validate($form, $form_state);

    $new_node = $form_state->getValue('new_node');

    if (empty($new_node)) {
      return;
    }

    if (!preg_match('/\[([0-9]+)\]$/', $new_node, $matches)) {
      $form_state->setErrorByName('new_node', $this->t('Please check & make sure that the node title is entered correctly'));
    }
    else {
      // Then make sure that the node is existing and accessible.
      $node_id = intval($matches[1]);

      if (!$this->entityManager->getStorage('node')->load($node_id)) {
        $form_state->setErrorByName('new_node', $this->t('Node with ID %nid can not be found.', array('%nid' => $node_id)));
      }
    }
  }

}
