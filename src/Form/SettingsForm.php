<?php

/**
 * @file
 * Contains \Drupal\node_disclaimer\Form\SettingsForm.
 */

namespace Drupal\node_disclaimer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_disclaimer_settings';
  }

  /**
   * @{inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['node_disclaimer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_disclaimer.settings');

    $form['show_on_node_form'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Show settings on node edit form'),
      '#description' => $this->t('Enable selecting the disclaimer for the node being created/updated'),
      '#default_value' => $config->get('show_on_node_form'),
    );

    $types = \Drupal\node\Entity\NodeType::loadMultiple();
    $types = array_keys($types);
    $types = array_combine($types, $types);

    $form['disclaimer_types'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Disclaimer node types'),
      '#description' => $this->t('Select node types that can be used as discalimers. Leave empty to select all.'),
      '#options' => $types,
      '#default_value' => $config->get('disclaimer_types'),
    );

    $form['protected_types'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->t('Protected node types'),
      '#description' => $this->t('Select node types that can be protected by disclaimers. Leave empty to select all.'),
      '#options' => $types,
      '#default_value' => $config->get('protected_types'),
    );

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // NOTE: Configuration is now stored in database by default rather than YML
    // files. Check: https://www.drupal.org/node/2241059.
    $this->config('node_disclaimer.settings')
      ->set('show_on_node_form', $form_state->getValue('show_on_node_form'))
      ->set('disclaimer_types', $form_state->getValue('disclaimer_types'))
      ->set('protected_types', $form_state->getValue('protected_types'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
