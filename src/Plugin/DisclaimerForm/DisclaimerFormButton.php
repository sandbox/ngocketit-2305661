<?php

/**
 * @file
 *
 * Contains
 * \Drupal\node_disclaimer\Plugin\DisclaimerType\DisclaimerFormButton.
 */

namespace Drupal\node_disclaimer\Plugin\DisclaimerForm;

use Drupal\node_disclaimer\DisclaimerFormBase;

/**
 * Defines a class for disclaimer type with checkbox & button.
 *
 * @DisclaimerForm(
 *   id = "disclaimer_form_button",
 *   label = @Translation("Disclaimer form with button")
 * )
 */
class DisclaimerFormButton extends DisclaimerFormBase {
  /**
   * Render the disclaimer form.
   */
  public function disclaimerForm() {
    $form = parent::disclaimerForm();

    $form['agree'] = array(
      '#type' => 'button',
      '#id' => 'disclaimer-accept',
      '#value' => t('Accept'),
    );

    return $form;
  }

  /**
   * Get the form element that triggers the acceptance.
   */
  public function getAcceptElement() {
    return array(
      'selector' => '#disclaimer-accept',
      'trigger'  => array('click'),
    );
  }
}
