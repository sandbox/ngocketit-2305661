<?php

/**
 * @file
 *
 * Contains
 * \Drupal\node_disclaimer\Plugin\DisclaimerType\DisclaimerFormCheckbox.
 */

namespace Drupal\node_disclaimer\Plugin\DisclaimerForm;

use Drupal\node_disclaimer\DisclaimerFormBase;

/**
 * Defines a disclaimer form with checkbox.
 *
 * @DisclaimerForm(
 *   id = "disclaimer_form_checkbox",
 *   label = @Translation("Disclaimer form with checkbox")
 * )
 */
class DisclaimerFormCheckbox extends DisclaimerFormBase {
   /**
   * Render disclaimer form.
   */
  public function disclaimerForm() {
    $form = $this->defaultDisclaimerForm();

    $form['agree'] = array(
      '#type' => 'checkbox',
      '#id' => 'disclaimer-accept',
      '#title' => t('I acknowledge and accept disclaimer'),
    );

    return $form;
  }

  /**
   * Get the form element that triggers the acceptance.
   */
  public function getAcceptElement() {
    return array(
      'selector' => '#disclaimer-accept',
      'trigger'  => array('click'),
    );
  }
}
