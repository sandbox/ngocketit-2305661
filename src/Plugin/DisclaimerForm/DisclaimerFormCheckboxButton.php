<?php

/**
 * @file
 *
 * Contains
 * \Drupal\node_disclaimer\Plugin\DisclaimerType\DisclaimerFormCheckboxButton.
 */

namespace Drupal\node_disclaimer\Plugin\DisclaimerForm;

use Drupal\node_disclaimer\DisclaimerFormBase;

/**
 * Defines a class for disclaimer type with checkbox & button.
 *
 * @DisclaimerForm(
 *   id = "disclaimer_form_checkbox_button",
 *   label = @Translation("Disclaimer form with checkbox & button")
 * )
 */
class DisclaimerFormCheckboxButton extends DisclaimerFormBase {
  /**
   * {@inheritdoc}
   */
   public function disclaimerForm() {
     $form = $this->defaultDisclaimerForm();

     $form['checkbox'] = array(
       '#type' => 'checkbox',
       '#id' => 'disclaimer-checkbox',
       '#title' => t('I acknowledge and accept the terms'),
     );

     $form['button'] = array(
       '#type' => 'button',
       '#id' => 'disclaimer-button',
       '#value' => t('Continue'),
     );

     $form['#attached'] = array(
       'library' => array(
         'node_disclaimer/node_disclaimer.checkbox_button',
       ),
     );

     return $form;
   }
}
